let cached = null

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomFromArray(arr) {
  let index = getRandomInt(0, arr.length-1)
  return arr[index]
}

function getRandomArrFromArray(arr) {
  let newLength = getRandomInt(1, arr.length-1)
  let newArr = []
  for (let i = 0; i < newLength; i++) {
    let index = getRandomInt(0, arr.length-1)
    newArr.push(arr[index])
  }
  return newArr
}

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

export function getRandomLessons(length) {

  return new Promise((resolve) => {

    if (cached) resolve(cached)
    
    else {  

      let lessons = []
      
      for (let i = 1; i <= length; i++) {
        let lesson = {}
        lesson.id = i
        lesson.drill = getDrill()
        lesson.debrief = getDebrief()
        lesson.subject = getSubject()
        lesson.subjectPlus = loremIpsumHebrew()
        lesson.stone = getStone()
        lesson.mission = loremIpsumHebrew()
        lesson.date = getDate()
        lesson.status = getStatus()
        lesson.statusPlus = loremIpsumHebrew()
        lesson.responsibility = getResponsibility()
    
    
        lessons.push(lesson)
      }
  
      cached = lessons
      // console.log(lessons)
      resolve(lessons)
    }

  })
  
}

function loremIpsumHebrew() {
  let lorems = [
    'לורם איפסום דולור סיט אמט, קונסקטורר',
    ' אדיפיסינג אלית קונדימנטום',
    'ורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.',
    'גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט',
    'ליבם סולגק. בראיט ולחת צורק',
    'מונחף, בגורמי מגמש.',
    ' תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.',
    'ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.',
    'לורם איפסום דולור סיט אמט',
    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור',
    'ליבם סולגק. בראיט ולחת צורק',
    'קראס אגת לקטוס וואל אאוגו',
    'וסטיבולום סוליסי',
    'טידום בעליק. קונדימנטום קורוס בליקרה',
    'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה.',
  ]

  return getRandomFromArray(lorems)
}

function getDrill() {
  let drills = [
    {
      name: "מבצע שומר החומות 2021",
      type: "מבצע",
      date: "10-5-2021"
    },
    {
      name: "תרגיל אבן פינה 2021",
      type: "תרגיל",
      date: "10-3-2021"
    },
    {
      name: "מבצע קרן אור 2020",
      type: "מבצע",
      date: "20-10-2020"
    }
  ]
  return getRandomFromArray(drills)
}

function getDebrief() {
  let debriefs = [
    'הסברה ותקשורת',
    'פו"ש',
  ]  
  return getRandomFromArray(debriefs)
}  

function getSubject() {
  let subjects = [
    'כללי',
    'אמצעים',
    'אוכלוסייה',
    'הסברה',
  ]  
  return getRandomFromArray(subjects)
}

function getStone() {
  let stones = [
    'ממשק פקע"ר עם מוקדים הרשותיים',
    'מיפוי ואבחון צרכי אוכלוסייה',
    'משפיעי דעת קהל',
    'אחידות המסר',
    'ציר פיקוד אורכי',
    'הפעלת כוחות',
  ]  
  return getRandomFromArray(stones)
}

function getDate() {
  let date = randomDate(new Date(2020, 1, 1), new Date(2022, 11, 31))
  let dateString = `${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`
  return {
    name: dateString,
    year: date.getFullYear()
  }
}

function getStatus() {
  let statuses = [
    'בוצע',
    'לא בוצע',
    'בביצוע',
  ]  
  return getRandomFromArray(statuses)
}

function getResponsibility() {
  let responsibilities = [
    'רמ"ח אוכ',
    'משקא"פ',
    'מפקדי מרכזים',
    'רע"ן תוהד',
    'רע"ן אגמים',
    'רע"ן עורף'
  ]  
  return getRandomArrFromArray(responsibilities)
}

