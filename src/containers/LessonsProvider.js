import React from 'react'

// custom hooks
import { useFetch } from '../hooks/useFetch'
import useFormatArr from '../hooks/useFormatArr'

// components
import Spinner from '../components/general/Spinner'

// context
export const LessonsContext = React.createContext([])

export function LessonsProvider({ children, baseUrl }) {
  
  const {data: lessons, isPending, error} = useFetch(baseUrl)
  const formattedLessons = useFormatArr(lessons)

  return (
    <LessonsContext.Provider value={{ lessons, formattedLessons }}>
        {/* pending */}
        {isPending && <Spinner size="6em" />}
        {/* error */}
        {error && <h3 style={{ textAlign: 'center'}}>{error}</h3>}
        {/* loaded */}
        {lessons && formattedLessons && children}
    </LessonsContext.Provider>
  )
}
