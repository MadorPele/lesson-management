import React, { useState, useEffect, useRef } from "react"
import styled from "styled-components"
import Spinner from '../components/general/Spinner'

const Container = styled.div`
  ${props => !props.transitionDone && 
    `transition: opacity ${TRANSITION_DURATION_MS}ms !important;`
  }
  opacity: ${props => props.loaded ? '1' : '0'};
`

const TRANSITION_DURATION_MS = 500

export default function DomLoader({ children, spinner, ...props }) {

  const [loaded, setLoaded] = useState(false)
  const [transitionDone, setTransitionDone] = useState(false)

  const timeOut = useRef(null)

  useEffect(() => {
    if (loaded) {
      timeOut.current = setTimeout(() => {
        setTransitionDone(true)
      }, TRANSITION_DURATION_MS);
    }
    return () => clearTimeout(timeOut.current)
  }, [loaded])


  return (
    <>
      {spinner && !loaded && <Spinner size="4em"/>}
      <Container 
        loaded={loaded} 
        transitionDone={transitionDone} 
        onLoad={() => setLoaded(true)} 
        {...props}
      >
        {React.Children.map(children, child => {
          if (React.isValidElement(child)) {
            return React.cloneElement(child)
          }
          return child
        })}
      </Container>
    </>
  )
}