import { useState, useEffect, useRef } from "react"

export default function DelayRender({ children, timeout=0 }) {

  const [render, setRender] = useState(false)
  const timeoutRef = useRef(null)

  useEffect(() => {
    timeoutRef.current = setTimeout(() => {
      setRender(true)
    }, timeout);

    return () => clearTimeout(timeoutRef.current)
  }, [])

  return render && children
}