import { useRef } from 'react'
import styled from 'styled-components'

// CSSTransition
import { CSSTransition } from 'react-transition-group'


const FadeDiv = styled.div`
  &.fade-up-appear {
    opacity: 0;
    transform: translateY(1rem);
  }

  &.fade-up-appear-active {
    opacity: 1;
    transform: translateY(0);
    transition: opacity 500ms, transform 500ms;
  }
`

export default function FadeUp({ children, inState = true, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-up"
    >
      <FadeDiv className='fade' ref={nodeRef} {...props}>
        {children}
      </FadeDiv>
    </CSSTransition>
  )
}
