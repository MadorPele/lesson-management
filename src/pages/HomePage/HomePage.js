// router
import { Link } from 'react-router-dom'

//containers
import FadeDown from '../../containers/FadeDown'
import ImgLoader from '../../components/general/ImgLoader'
import ImagesLoader from '../../containers/ImagesLoader'

// styles
import './HomePage.scss'

// images
import PakarLogo from '../../assets/PakarBlack.svg'
import SiteLogo from '../../assets/logo.svg'
import SiteLogo2 from '../../assets/logo2.svg'

import Spreadsheets from '../../assets/spreadsheet.svg'
import Data from '../../assets/data.svg'
import Personal from '../../assets/personal.svg'

// showcase items
const items = [
  { img: Personal, text: 'הלקחים שלי', to: '/me' },
  { img: Spreadsheets, text: 'טבלת לקחים', to: '/table' },
  { img: Data, text: 'תמונת מצב לקחים', to: '/stats' },
]

export default function HomePage() {
  
  return (
    <ImagesLoader spinner style={{ height: '100%', width: '100%' }}>
      <div className='home-page'>
        <div className="logos">
          <ImgLoader className='logo pakar-logo' src={PakarLogo} spinner={'4rem'} />
          <ImgLoader className='logo site-logo' src={SiteLogo} />
          <ImgLoader className='logo site-logo-2' src={SiteLogo2} />
        </div>
        <FadeDown>
          <div className="nav-panel">
            {items.map(item => (
              <Link to={item.to} className="item" key={item.text}>
                <ImgLoader src={item.img} spinner={'4rem'}/>
                <div className="sep-line"></div>
                <p>{item.text}</p>
              </Link>
            ))}
          </div>
        </FadeDown>
      </div>
    </ImagesLoader>
  )
}
