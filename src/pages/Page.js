import { useEffect } from "react"

// components
import FadeDown from "../containers/FadeDown"

export default function Page({ component: Component, title }) {
  
  // set document title
  useEffect(() => {
    document.title = title
  },[])

  return (
    <FadeDown style={{ height: '100%', width: '100%'}}>
      <div className="page">
        <Component />
      </div>
    </FadeDown>
  )
}
