import styled from 'styled-components'

// custom hooks
import useLessons from '../../hooks/useLessons'
import useSortArr from '../../hooks/useSortArr'

// components
import TableWithFilterBar from '../../components/lesson_table/TableWithFilterBar'

// params to show on filter bar
const filterParams = ['drill','debrief','subject','stone','status','responsibility']

// styles
const TablePageCon = styled.div`
  height: 100%;
  width: 100%;
`
const Content = styled.div`
  padding: 0 1em;
`

export default function TablePage() {

  const { formattedLessons } = useLessons()
  const sortedLessons = useSortArr(formattedLessons, 'drill')

  return (
    <TablePageCon>
      <h2 className="title-top">טבלת לקחים</h2>
      <Content>
        <TableWithFilterBar
          tableArr={sortedLessons}
          filterParams={filterParams}
        />
      </Content>
    </TablePageCon>
  )
}
