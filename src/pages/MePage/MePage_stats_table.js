// custom hooks
import useLessons from '../../hooks/useLessons'
import useSortArr from '../../hooks/useSortArr'
import useFilterArr from '../../hooks/useFilterArr'

// components
import TableWithFilterBar from '../../components/lesson_table/TableWithFilterBar'

export default function MePage_stats_table({ picked }) {

  const { formattedLessons } = useLessons()
  const sortedArr = useSortArr(formattedLessons, 'drill')
  const filteredArr = useFilterArr(sortedArr, {responsibility: picked})

  return (
    <>
      <h3 className='title' style={{ marginTop: '2em'}}>
        כלל הלקחים של המחלקה באחריות <b>{picked}</b>
      </h3>
      <TableWithFilterBar 
        tableArr={filteredArr}
        filterParams={['drill']}
      />
    </>
  )
}