import styled from 'styled-components'

// custom hooks
import useUrlParam from '../../hooks/useUrlParam'

// containers
import FadeDown from '../../containers/FadeDown'

// components
import MePage_title from './MePage_title'
import MePage_picker from './MePage_picker'
import MePage_stats from './MePage_stats'
import BackTitle from '../../components/general/BackTitle'

// styles
const MePageCon = styled.div`
  width: 100%;
`

export default function MePage() {

  const [picked, setPicked] = useUrlParam('picked')

  return (
    <MePageCon>
      <BackTitle 
        text="הלקחים שלי"
        showButton={picked}
        handleClick={() => setPicked('')}
      />
      {!picked && <FadeDown><MePage_picker setPicked={setPicked} /></FadeDown> }
      {picked && <FadeDown><MePage_stats picked={picked} /></FadeDown>}
    </MePageCon>
  )
}
