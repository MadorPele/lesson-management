import { useState, useEffect } from "react"

// components
import Picker from "../../components/general/Picker"

// hooks
import useLessons from '../../hooks/useLessons'

export default function MePage_picker({ setPicked }) {

  const { getValues } = useLessons()
  const [options, setOptions] = useState(null)

  useEffect(() => {
    setOptions(getValues('responsibility'))
  }, [])

  const handleClick = option => {
    setPicked(option)
  }
  
  return (
    <>
      {options && (
        <Picker options={options} handleClick={handleClick} />
      )}
    </>
  )
}