import { useEffect } from "react";

// router
import { useParams } from "react-router-dom"

// custom hooks
import useFilterArr from "../../hooks/useFilterArr"
import useLessons from '../../hooks/useLessons'

// components
import TextToCopy from '../../components/general/TextToCopy'
import Table from "../../components/lesson_table/Table"

// styles
import './LessonPage.scss'

export default function LessonPage() {
  
  const { formattedLessons } = useLessons()
  const { id } = useParams()
  const lesson = useFilterArr(formattedLessons, { id })
  
  // set document title
  useEffect(() => {
    document.title = 'מנה"ל - לקח ' + id
  },[id, document.title])
  
  return lesson.length ? (
    <div className="lesson-page">
      <h2 className="title-top">
        לקח {lesson[0]?.id}
      </h2>
      <div className="content">
        <Table 
          tableArr={lesson} 
          clickable={false} 
          listMode={true}
        />
        <div className="share">
          <span>שתף לקח זה</span>
          <TextToCopy text={window.location.href} />
        </div>
      </div>
    </div>
  ) : <h2>לקח {id} לא נמצא</h2>
}
