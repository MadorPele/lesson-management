// custom hooks
import useLessons from '../../hooks/useLessons'
import useStatusStats from '../../hooks/useStatusStats'

// components
import Graphs from '../../components/lesson_stats/Graphs'

export default function StatsPage_graphs(props) {

  const { lessons } = useLessons()
  const responsibilityStatusStats = useStatusStats(lessons, {}, 'responsibility')

  const bottom3 = responsibilityStatusStats.slice(-3)
  const top3 = responsibilityStatusStats.slice(0,3)

  const bottom3_graphData = bottom3.reduce((arr, item) => ([ ...arr, {name: item.key, value: item.valuesPercentage.green.toFixed(1)}]), [])
  const top3_graphData = top3.reduce((arr, item) => ([ ...arr, {name: item.key, value: item.valuesPercentage.green.toFixed(1)}]), [])

  const graphs = [
    {
      title: {normal: 'שלושת בעלי התחום בעלי אחוז הלקחים המיושמים ', bold: 'הנמוך ביותר'},
      data: bottom3_graphData
    },
    {
      title: {normal: 'שלושת בעלי התחום בעלי אחוז הלקחים המיושמים ', bold: 'הגבוה ביותר'},
      data: top3_graphData
    },
  ]

  return (
    <Graphs 
      graphs={graphs}
      style={{
        margin: '2em auto',
        width: '100%',
        maxWidth: '100vw',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        gap: '5vmax'
      }}
      graphStyle={{
        width: '35em',
        height: '25em',
        maxWidth: '95vw',
      }}
      {...props}
    />
  )
}