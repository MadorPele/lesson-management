import styled from 'styled-components'

// components
import StatsPage_pies from './StatsPage_pies'
import StatsPage_graphs from './StatsPage_graphs'

// containers
import FadeDown from '../../containers/FadeDown'
import DelayRender from '../../containers/DelayRender'

// styles
const StatsPageCon = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-rows: auto 1fr;
  .fade {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`
  
const Content = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  place-items: center;
`

export default function StatsPage() {

  return (
    <StatsPageCon>
      <h2 className="title-top">תמונת מצב לקחים</h2>
      <DelayRender>
        <FadeDown>
          <Content>
            <StatsPage_pies />
            <FadeDown>
              <StatsPage_graphs />
            </FadeDown>
          </Content>
        </FadeDown>
      </DelayRender>
    </StatsPageCon>
  )
}
