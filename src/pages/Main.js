import styled from 'styled-components'

//router 
import { Routes, Route, Navigate } from 'react-router-dom'

// pages
import Page from './Page'
import HomePage from './HomePage/HomePage'
import TablePage from './TablePage/TablePage'
import StatsPage from './StatsPage/StatsPage'
import MePage from './MePage/MePage'
import FilterPage from './FilterPage/FilterPage'
import LessonPage from './LessonPage/LessonPage'

import TopScroller from '../components/general/TopScroller'

const pages = [
  {id: 1, path: '/home',  title: 'מנה"ל - עמוד הבית',  component: HomePage},
  {id: 2, path: '/table', title: 'מנה"ל - טבלת לקחים', component: TablePage },
  {id: 3, path: '/stats',  title: 'מנה"ל - תמונת מצב לקחים',  component: StatsPage},
  {id: 4, path: '/me',  title: 'מנה"ל - הלקחים שלי',  component: MePage},
  {id: 5, path: '/filter',  title: 'מנה"ל - מיון אירוע/שנה',  component: FilterPage},
  {id: 6, path: '/lessons/:id',  title: 'מנה"ל - לקח',  component: LessonPage},
]

const MainCon = styled.div`
  overflow: auto;
  height: 100%;
  width: 100%;
`

export default function Main() {

  return (
    <MainCon>
        <Routes>
          {pages.map(page => (
            <Route 
              key={page.id} 
              path={page.path} 
              element={
                <Page 
                  key={page.id}
                  title={page.title}
                  component={page.component}
                />
              }
            />
          ))}
          <Route path="*" element={<Navigate to="/home" />}/>
        </Routes>
      <TopScroller />
    </MainCon>
  )
}
