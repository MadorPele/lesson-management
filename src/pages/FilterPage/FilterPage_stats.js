// custom hooks
import useLessons from "../../hooks/useLessons"
import useFilterArr from "../../hooks/useFilterArr"
import useStatusStats from "../../hooks/useStatusStats"

// containers
import DelayRender from "../../containers/DelayRender"

// components
import StyledContainer from "../../components/general/StyledContainer"
import Pies from "../../components/lesson_stats/Pies"
import Graph from "../../components/lesson_stats/Graph"

export default function FilterPage_stats({ mainKey, subKey, value }) {

  const { lessons } = useLessons()
  const filteredArr = useFilterArr(lessons, {[mainKey]: value}, subKey)

  // pie
  const { valuesPercentage: pieData } = useStatusStats(filteredArr, {})
  const pie = { 
		title: {
			normal: 'התפלגות סטטוס יישום ', 
			bold: `כלל הלקחים מ${value}`
		}, 
    greenValue: pieData.green, 
    yellowValue: pieData.yellow, 
    redValue: pieData.red 
  }

  // graph 1
  const responsibilityStatusStats = useStatusStats(filteredArr, {}, 'responsibility')
  const graphData_1 = responsibilityStatusStats.reduce((arr, item) => ([ ...arr, {name: item.key, value: item.valuesPercentage.green.toFixed(1)}]), [])
  const graph_1 = {
    title: {
      normal: `התפלגות יישום לקחים `, 
      bold: 'ע"פ אחריות'
    },
    data: graphData_1
  }
  
  // graph 2
  const subjectStatusStats = useStatusStats(filteredArr, {}, 'subject')
  const graphData_2 = subjectStatusStats.reduce((arr, item) => ([ ...arr, {name: item.key, value: item.valuesPercentage.green.toFixed(1)}]), [])
  const graph_2 = {
    title: {
      normal: `התפלגות יישום לקחים `, 
      bold: 'ע"פ נושאים'
    },
    data: graphData_2
  }

  return (
    <>
      <h3 className="title-top bright">{value}</h3>
      <DelayRender>
        <Pies pies={[pie]} style={{
          display: 'flex',
          justifyContent: 'center'
        }}/>
        <StyledContainer>
          <Graph graph={graph_1} style={{
            width: '35em',
            height: '25em',
            maxWidth: '100%',
            marginTop: '1em'
          }}/>
          <Graph graph={graph_2} style={{
            width: '35em',
            height: '25em',
            maxWidth: '100%',        
            marginTop: '1em'
          }}/>
        </StyledContainer>
      </DelayRender>
    </>
  )
}