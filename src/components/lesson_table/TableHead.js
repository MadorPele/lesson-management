export default function TableHead() {
  return (
    <thead>
      <tr>
        <td>סימוכין</td>
        <td>תרגיל/מבצע</td>
        <td>תחקיר</td>
        <td>נושא</td>
        <td>תיאור נושא</td>
        <td>אבן בניין הכוח</td>
        <td>משימה</td>
        <td>תג"ב</td>
        <td>סטטוס</td>
        <td>הערות סטטוס</td>
        <td>אחריות</td>
      </tr>
    </thead>
  )
}
