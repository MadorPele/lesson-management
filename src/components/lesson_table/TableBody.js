import TableRow from './TableRow'

export default function TableBody({ tableArr, clickable }) {
  return (
    <tbody>
      {tableArr.map(row =>
        <TableRow 
          key={row.id} 
          row={row} 
          clickable={clickable} 
        />
      )}
    </tbody>
  )
}