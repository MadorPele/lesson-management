import { useState, useEffect } from 'react'

// containers
import FadeDown from '../../containers/FadeDown'

// custom hooks
import useWindowDimensions from '../../hooks/useWindowDimentions'

// components
import TableHead from './TableHead'
import TableBody from './TableBody'
import List from './List'
import Switch from '../general/Switch'

// styles
import './Table.scss'

export default function Table({ tableArr, clickable, listMode = false }) {

  const listModeState = useState(listMode);
  const [isListMode, setListMode] = listModeState

  const { width } = useWindowDimensions();

  useEffect(() => {
    width <= 1100 && setListMode(true)
  },[width])

  return (
    <>
    <div className="table-switch">
      <Switch 
        state={listModeState}
        label="החלף בין טבלה לרשימה"
        offLabel="טבלה"
        onLabel="רשימה"
      />
    </div>
    {!isListMode && (
      <FadeDown style={{ width: '100%' }}>
        <table className="table">
          <TableHead />
          <TableBody
            tableArr={tableArr}
            clickable={clickable}
            />
        </table>
      </FadeDown>
    )}
    {isListMode && (
      <FadeDown style={{ width: '100%' }}>
        <div className="table-list">
          <List
            listArr={tableArr}
            clickable={clickable}
          />
        </div>
      </FadeDown>
    )}
    </>
  )
}