//router
import { useNavigate } from 'react-router-dom'

// custom hooks
import getHs from '../../hooks/getHs'

export default function ListItem({ item, clickable }) {

  const navigate = useNavigate()

  const handleClick = () => {
    clickable && navigate(`/lessons/${id}`)
  }
  
  const handleKeyPress = e => {
    e.key == 'Enter' && clickable && navigate(`/lessons/${id}`)
  }

  const {
    id,
    drill,
    debrief,
    subject,
    subjectPlus,
    stone,
    mission,
    date,
    status,
    statusPlus,
    responsibility
  } = item

  return (
    <li
      className={clickable ? "clickable" : "not-clickable"}
      onClick={() => handleClick(item.id)}
      tabIndex={clickable ? '0' : null}
      onKeyPress={handleKeyPress}
    >
      <h2>סימוכין: {id}</h2>

      <dl>
        <dt>תרגיל/מבצע</dt>
        <dd className="colored" style={{'--main-hs': getHs(drill)}}>{drill}</dd>

        <dt>תחקיר</dt>
        <dd>{debrief}</dd>

        <dt>נושא</dt>
        <dd>{subject}</dd>

        <dt>תיאור נושא</dt>
        <dd>{subjectPlus}</dd>

        <dt>אבן בניין הכוח</dt>
        <dd>{stone}</dd>

        <dt>משימה</dt>
        <dd>{mission}</dd>

        <dt>תג"ב</dt>
        <dd>{date}</dd>

        <dt>סטטוס</dt>
        <dd className="colored" style={{'--main-hs': getHs(status)}}>{status}</dd>

        <dt>הערות סטטוס</dt>
        <dd>{statusPlus}</dd>

        <dt>אחריות</dt>
        <dd>{responsibility}</dd>
      </dl>
    </li>
  )
}
