import ListItem from "./ListItem"

// styles
import './List.scss'


export default function List({ listArr, clickable }) {
  return (
    <ul>
      {listArr.map(item => 
        <ListItem 
          key={item.id}
          item={item}
          clickable={clickable}
        />
      )}
    </ul>
  )
}