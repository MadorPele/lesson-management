import { useState, useEffect } from 'react'

// components
import Table from './Table'
import FilterBar from './FilterBar'
import IncrementBtn from './IncrementBtn'

// custom hooks
import useUrlParam from '../../hooks/useUrlParam'
import useFilterArr from '../../hooks/useFilterArr'
import useSliceArr from '../../hooks/useSliceArr'

export default function TableWithFilterBar({ tableArr, filterParams }) {
  
  const [queryObj, setQueryObj] = useUrlParam('filter',{})
  const filteredArr = useFilterArr(tableArr, queryObj)
  const { slicedArr, incrementSlice, resetSlice, isFull } = useSliceArr(filteredArr)

  // reset slice on filter change
  useEffect(() => {
    resetSlice()
  }, [filteredArr])

  return (
    <>
      {slicedArr.length ?
      <> 
      {filterParams && (
        <FilterBar
          filterParams={filterParams}
          arr={filteredArr} 
          queryObj={queryObj} 
          setQueryObj={setQueryObj}
        />
      )}
        <Table
          tableArr={slicedArr}
          clickable={true}
        />
        <IncrementBtn 
          onClick={incrementSlice}
          sliceInfo={`${slicedArr.length}/${filteredArr.length}`} 
          isFull={isFull}
        />
      </>
      : null}
    </>
  )
}
