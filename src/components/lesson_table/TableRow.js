//router
import { useNavigate } from 'react-router-dom'

// custom hooks
import getHs from '../../hooks/getHs'

export default function TableRow({ row, clickable }) {

  const navigate = useNavigate()

  const handleClick = id => {
    clickable && navigate(`/lessons/${id}`)  
  }
  
  const handleKeyPress = e => {
    e.key === 'Enter' && clickable && navigate(`/lessons/${id}`)
  }

  const {
    id,
    drill,
    debrief,
    subject,
    subjectPlus,
    stone,
    mission,
    date,
    status,
    statusPlus,
    responsibility
  } = row

  return (
    <tr 
      className={clickable ? "clickable" : "not-clickable"}
      onClick={() => handleClick(id)}
      tabIndex={clickable ? '0' : null}
      onKeyPress={handleKeyPress}
    >
      <td>{id}</td>
      <td className="colored" style={{'--main-hs': getHs(drill)}}>{drill}</td>
      <td>{debrief}</td>
      <td>{subject}</td>
      <td>{subjectPlus}</td>
      <td>{stone}</td>
      <td>{mission}</td>
      <td>{date}</td>
      <td className="colored" style={{'--main-hs': getHs(status)}}>{status}</td>
      <td>{statusPlus}</td>
      <td>{responsibility}</td>
    </tr>
  )
}
