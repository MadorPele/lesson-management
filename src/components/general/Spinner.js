// spinner
import { SpinnerCircular } from 'spinners-react';

// styles
const style = {
  display: 'flex',
  justifyContent: 'center'
}

export default function Spinner({ size }) {
  return (
    <div style={style}>
      <SpinnerCircular
        size={size} 
        thickness={100} 
        speed={130} 
        color="#5B6880" 
        secondaryColor="rgba(0, 0, 0, 0)" 
      />
    </div>
  )
}
