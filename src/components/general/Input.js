
export default function Input({ modelState, ...props }) {

  if (modelState) {
    const [state, setState] = modelState
    return <input value={state} onChange={e => setState(e.target.value)} { ...props} />
  } 
  
  else return <input { ...props} />
}