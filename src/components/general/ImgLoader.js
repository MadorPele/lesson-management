import { useState, useEffect, useRef } from "react"
import styled from "styled-components"
import Spinner from './Spinner'

const Img = styled.img`
  ${props => !props.loaded && 
    `position: absolute;`
  }
  ${props => !props.transitionDone && 
    `transition: opacity ${TRANSITION_DURATION_MS}ms !important;`
  }
  opacity: ${props => props.loaded ? '1' : '0'}
`

const TRANSITION_DURATION_MS = 500

export default function ImgLoader({ spinner, ...props }) {

  const [loaded, setLoaded] = useState(false)
  const [transitionDone, setTransitionDone] = useState(false)

  const timeOut = useRef(null)

  useEffect(() => {
    if (loaded) {
      timeOut.current = setTimeout(() => {
        setTransitionDone(true)
      }, TRANSITION_DURATION_MS);
    }
    return () => clearTimeout(timeOut.current)
  }, [loaded])
  

  return (
    <>
    {spinner && !loaded && <Spinner size={spinner}/>}
      <Img 
        loaded={loaded}
        transitionDone={transitionDone}
        onLoad={() => setLoaded(true)}
        {...props}
      />
    </>
  )
}