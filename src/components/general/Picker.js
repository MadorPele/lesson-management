// styles
import './Picker.scss'

export default function Picker({ options = [], handleClick }) {

  return (
    <div className="picker-options">
      {options.map((option, index) => (
        <button 
          key={index} 
          className="option btn-reset" 
          onClick={() => handleClick(option)}
        >
          {option}
        </button>
      ))}
    </div>
  )
}