// styles
import './StyledContainer.scss'

export default function StyledContainer({ children, ...props }) {
  return (
    <div {...props} className={`styled-container ${ props.className || '' }`}>
      {children}
    </div>
  )
}