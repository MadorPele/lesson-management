// components
import Pie from './Pie'

export default function Pies({ pies, ...props }) {

  return (
    <div className="pies" {...props}>
      {pies.map((pieData, index) => {
        return (
          <Pie key={index} pieData={pieData}/>
        )
      })}
    </div>
  )
}