import { PieChart } from 'react-minimal-pie-chart'

// containers
import FadeDown from '../../containers/FadeDown'

// styles
import './Pie.scss'

export default function Pie({ pieData }) {

  const { title, greenValue, yellowValue, redValue } = pieData

  const segments = [
    {value: greenValue, title: 'בוצע', color: 'var(--green)'}, 
    {value: yellowValue, title: 'בביצוע', color: 'var(--yellow)'}, 
    {value: redValue, title: 'לא בוצע', color: 'var(--red)'}
  ].sort((a, b) =>  a.value - b.value).reverse()

  return (
    <div className="pie-con">
      <h3 className="pie-title">{title.normal}<b>{title.bold}</b></h3>
      <PieChart
        className="pie"
        animate
        animationEasing="ease"
        animationDuration="750"
        rounded
        lineWidth={60}
        paddingAngle={20}
        data={segments}
      />
      <FadeDown>
        <dl className="pie-info">
          <dd style={{ '--color': 'var(--green)'}}>{greenValue.toFixed(1)}%</dd>
          <dt>בוצע</dt>

          <dd style={{ '--color': 'var(--yellow)'}}>{yellowValue.toFixed(1)}%</dd>
          <dt>בביצוע</dt>

          <dd style={{ '--color': 'var(--red)'}}>{redValue.toFixed(1)}%</dd>
          <dt>לא בוצע</dt>
        </dl>
      </FadeDown>
    </div>
  )
}
