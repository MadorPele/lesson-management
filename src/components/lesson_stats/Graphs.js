// components
import Graph from './Graph'

export default function Graphs({ graphs, graphStyle, ...props }) {

  return (
    <div {...props}>
      {graphs.map((graph, index) => {
        return (
          <Graph key={index} graph={graph} style={graphStyle}/>
        )
      })}
    </div>
  )
}