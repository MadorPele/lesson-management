import { ResponsiveContainer, XAxis, YAxis, BarChart, Bar, LabelList, Tooltip } from 'recharts'

// styles
import './Graph.scss'

export default function Graph({ graph, ...props }) {

  const { title, data } = graph
  const getLabelValue = label => label.value+'%'
  
  const renderTooltip = ({ label, payload }) => {
    return (
      <div className='tooltip'>
        <div>{label}</div>
        <div>{payload[0]?.value}%</div>
      </div>
    )
  }

  return (
    <div className="graph-con">
      <div {...props}>
        <ResponsiveContainer width="100%" height="100%">
          <BarChart data={data} barCategoryGap="20%" className="graph">
            <XAxis dataKey="name" />
            <YAxis type="number" domain={[0, 100]} unit="%" />
            <Bar dataKey="value" radius={[10, 10, 0, 0]} className="bar">
              <LabelList valueAccessor={getLabelValue} position="top" />
            </Bar>
            <Tooltip cursor={false} content={renderTooltip} />
          </BarChart>
        </ResponsiveContainer>
      </div>
      <h3 className="title">{title.normal} <b>{title.bold}</b></h3>
    </div>
  )
}