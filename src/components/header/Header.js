// router
import { Link } from 'react-router-dom'

// components
import NavBar from './NavBar'
import ThemeButton from './themeButton'

// styles
import './Header.scss'

//images
import PakarLogo from '../../assets/PakarBlack.svg'
import SmallLogo from '../../assets/logo.svg'

export default function Header({ lightMode, setLightMode }) {

  return (
    <header>
        <Link to="/home">
          <img src={SmallLogo} className="small-logo logo"/>
        </Link>
        <ThemeButton lightMode={lightMode} setLightMode={setLightMode} />
        <NavBar />
        <img src={PakarLogo} className="pakar-logo logo"/>
    </header>
  )
}
