import { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'

// components
import Header from './components/header/Header'
import Main from './pages/Main'

// containers
import AppErrorBoundary from './containers/AppErrorBoundary'

// context
import { LessonsProvider } from './containers/LessonsProvider'

// styles
import './App.scss'


export default function App() {
  
  const [lightMode, setLightMode] = useState(true);

  return (
    <div className={`App ${ lightMode ? 'light-mode' : 'dark-mode'}`}>
      <BrowserRouter basename={getRouterBasename()}>
        <AppErrorBoundary>
          <Header lightMode={lightMode} setLightMode={setLightMode} />
          <LessonsProvider baseUrl={baseUrl}>
            <Main />
          </LessonsProvider>
        </AppErrorBoundary>
      </BrowserRouter>
    </div>
  )
}

//* config: *//

// database url (leave empty for fake data)
const baseUrl = ''

// config router for deployment
const getRouterBasename = () => {
  if (process.env.NODE_ENV == 'production' && process.env.PUBLIC_URL) 
    return new URL(process.env.PUBLIC_URL).pathname
  return '/'
}