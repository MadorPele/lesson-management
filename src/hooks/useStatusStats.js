import useFilterArr from './useFilterArr'

export default function useStatusStats( arr, query = {}, key ) {

  const filteredArr = useFilterArr(arr, query)

  const toPrecentages = (obj, sum) => {
    if (!sum) {
      let sum = 0;
      let newObj = {};
      Object.values(obj).forEach(value => {
        sum += value
      })
      Object.keys(obj).forEach(key => {
        newObj[key] = (obj[key]/sum) * 100 || 0
      })
      return newObj
    } else {
      let newObj = {};
      Object.keys(obj).forEach(key => {
        newObj[key] = (obj[key]/sum) * 100 || 0
      })
      return newObj
    }
  }
  
  if (key) {
    let keyStatsObj = {}

    const countKey = (item, subKey) => {
      if (!keyStatsObj[subKey]) keyStatsObj[subKey] = {
        green: 0,
        yellow: 0,
        red: 0,
        sum: 0
      }
      keyStatsObj[subKey].sum++
      switch (item.status) {
        case 'בוצע': keyStatsObj[subKey].green++; break
        case 'בביצוע': keyStatsObj[subKey].yellow++; break
        case 'לא בוצע': keyStatsObj[subKey].red++; break
      }
    }

    const arrSortedByGreenPercentage = (obj) => {
      const keys = Object.keys(obj)
      keys.sort((a, b) => {
        if (obj[a].sum/obj[a].green < obj[b].sum/obj[b].green) return -1
        if (obj[a].sum/obj[a].green > obj[b].sum/obj[b].green) return 1
        return 0
      })
      const keyStatsArr = []
      keys.forEach(key => {
        keyStatsArr.push({
          key: key,
          values: obj[key],
          valuesPercentage: {
            green: (obj[key].green/obj[key].sum)*100,
            yellow: (obj[key].yellow/obj[key].sum)*100,
            red: (obj[key].red/obj[key].sum)*100
          },
        })
      })
      return keyStatsArr
    }

    filteredArr.forEach(item => {
      if (Array.isArray(item[key])) {
        item[key].forEach(subKey => {
          countKey(item, subKey)
        })
      } else if (typeof item[key] == 'object') {
        countKey(item, item[key].name)
      } else {
        countKey(item, item[key])
      }
    })
    return arrSortedByGreenPercentage(keyStatsObj)

  } else {
    let green = 0, yellow = 0, red = 0, sum = 0

    filteredArr.forEach(item => {
      sum++
      switch (item.status) {
        case 'בוצע': green++; break
        case 'בביצוע': yellow++; break
        case 'לא בוצע': red++; break
      }
    })
    return {
      values: {green, yellow, red, sum},
      valuesPercentage: toPrecentages({green, yellow, red}, sum)
    }
  }
}