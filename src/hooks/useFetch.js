import { useState, useEffect } from "react"
import { getRandomLessons } from '../data'

const savedFetches = []

const FAKE_DATA_LENGTH = 1000

export const useFetch = (url) => {
  const [data, setData] = useState(null)
  const [isPending, setIsPending] = useState(false)
  const [error, setError] = useState(null)

  
  useEffect(() => {

    if (url) {

      if (savedFetches[url]) {
        setData(savedFetches[url])
      }

      // fetch from server
      else {
        const controller = new AbortController()
        
        (async () => {
          setIsPending(true)
          
          try {
            const res = await fetch(url, { signal: controller.signal })
            if(!res.ok) {
              throw new Error(res.statusText)
            }
            const data = await res.json()
    
            setIsPending(false)
            savedFetches[url] = data
            setData(data)
            setError(null)
          } catch (err) {
            if (err.name === "AbortError") {
              console.log("the fetch was aborted")
            } else {
              setIsPending(false)
              setData([])
              console.error('Could not fetch the data:\n', err);
              setError('Could not fetch the data')
            }
          }
        })()
        return () => {
          controller.abort()
        }
      }
      
    // fake data
    } else {
      (async () => {
        setIsPending(true)
        const data = await getRandomLessons(FAKE_DATA_LENGTH)
        setTimeout(() => {
          setData(data)
          setIsPending(false)
        }, 500);
      })()
    }
  }, [url])

  return { data, isPending, error }
}