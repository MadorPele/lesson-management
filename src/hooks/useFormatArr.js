import { useState, useEffect } from "react"

export default function useFormatArr(arr) {
  
  const [formattedArr, setFormattedArr] = useState(null);

  useEffect(() => {
    if (arr) {
      setFormattedArr(formatArr(arr))
    }
  },[arr])
  
  // formats arrays and objects in array to strings
  const formatArr = (arr) => {
    const newArr = []
    arr.forEach(row => {
      const newRow = {...row}
      Object.keys(newRow).forEach(key => {

        // if array, join with commas
        if (Array.isArray(newRow[key])) {
          newRow[key] = newRow[key].join(', ')
        }
        
        // if object, return name prop
        if (typeof newRow[key] == 'object') {
          newRow[key] = newRow[key].name
        }
      })
      newArr.push(newRow)
    })
    return newArr
  }
  
  return formattedArr
}
