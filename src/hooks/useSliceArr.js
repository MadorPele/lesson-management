import { useEffect, useState } from "react"

const START_NUM = 20
const INCREMENT_NUM = 100

export default function useSliceArr(arr) {

  const [slicedArr, setSlicedArr] = useState([])
  const [rowsToShow, setRowsToShow] = useState(START_NUM)
  const [isFull, setIsFull] = useState(false);
  
  const incrementSlice = () => {
    setRowsToShow(prev => prev + INCREMENT_NUM)
  }

  const resetSlice = () => {
    setRowsToShow(START_NUM)
  }

  // update slicedArr when rowsToShow changes
  useEffect(() => {
    setSlicedArr(arr.slice(0, rowsToShow))
  }, [rowsToShow, arr])
  
  // check if full
  useEffect(() => {
    if (slicedArr.length == arr.length && arr.length != 0) {
      setIsFull(true)
    } else {
      setIsFull(false)
    }
  }, [arr, slicedArr])

  return { slicedArr, incrementSlice, resetSlice, isFull }
}
