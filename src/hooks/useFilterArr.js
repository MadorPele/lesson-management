import { useState, useEffect } from "react"

export default function useFilterArr(dataArr, queryObj, subKey) {
  
  const [filteredArr, setFilteredArr] = useState([])

  useEffect(() => {
    if (dataArr) {
      setFilteredArr(filterArray(dataArr, queryObj, subKey))
    }
  }, [JSON.stringify(dataArr), JSON.stringify(queryObj)])
  
  const filterArray = (dataArr, queryObj, subKey) => {
    if (dataArr.length) {
      if (!Object.keys(queryObj).length) return dataArr
      const filteredArray = dataArr.filter(item => {
        let bool = true
        Object.keys(queryObj).forEach(key => {
          let bool2 = false
          if (typeof item[key] == 'object' && !Array.isArray(item[key])) {
            if (subKey) {
              if (item[key][subKey] == queryObj[key]) bool2 = true
            } else {
              if (JSON.stringify(item[key]) == JSON.stringify(queryObj[key])) bool2 = true
            }
          } else {
            String(item[key]).split(',').forEach(item => {
              item = item.trim()
              if (item == queryObj[key]) bool2 = true
            })
          }
          if (!bool2) bool = false
        })
        return bool
      })
      return filteredArray
    } else {
      return []
    }
  }

  return filteredArr
}